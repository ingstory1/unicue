# uniCue
## Getting started
uuid v4 만큼의 무작위성에 가깝게 16자리로 생성이 필요하여 만듦
1초에 최대 약 6.510^10개의 고유한 값을 생성할 수 있음.

```
phpize
./configure --enable-uniCue
make
sudo make install
```
