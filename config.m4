PHP_ARG_ENABLE(uniCue, whether to enable "uniCue" support, [ --enable-uniCue   Enable "uniCue" support])

if test "$PHP_UNICUE" != "no"; then
  PHP_NEW_EXTENSION(uniCue, uniCue.c, $ext_shared)
fi

