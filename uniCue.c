#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include <uuid/uuid.h>

#define PHP_UNICUE_VERSION "0.1"
#define PHP_UNICUE_EXTNAME "uniCue"

PHP_FUNCTION(uniCue);

zend_function_entry uniCue_functions[] = {
    PHP_FE(uniCue, NULL)
    {NULL, NULL, NULL}
};

zend_module_entry uniCue_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_UNICUE_EXTNAME,
    uniCue_functions,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#if ZEND_MODULE_API_NO >= 20010901
    PHP_UNICUE_VERSION,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_UNICUE
ZEND_GET_MODULE(uniCue)
#endif

PHP_FUNCTION(uniCue) {
    uuid_t uuid;
    char uuid_str[37];  // 36 characters + null character
    char result_str[17]; // Resultant 16 characters UUID string

    // Generate a UUID
    uuid_generate(uuid);

    // Convert the UUID to a string
    uuid_unparse_lower(uuid, uuid_str);

    // Copy the first 16 characters of the UUID without "-"
    int j = 0;
    for(int i = 0; i < 36 && j < 16; i++){
        if(uuid_str[i] != '-'){
            result_str[j] = uuid_str[i];
            j++;
        }
    }

    result_str[16] = '\0';
    RETURN_STRING(result_str);
}
